package main

import (
	"fmt"
	"math"
	"os"
	"time"

	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
)

const (
	fontPath = "font.ttf"
	fontSize = 32
)

var winTitle = "Go-SDL2 Render"
var winWidth, winHeight int32 = 800, 600
var startSimulation time.Time
var pixelPerMeter float64 = 33.0

var datachannel chan DeltaData

func run() int {
	var window *sdl.Window
	var renderer *sdl.Renderer
	var err error
	datachannel = make(chan DeltaData)

	if err = ttf.Init(); err != nil {
		return 1
	}
	defer ttf.Quit()

	window, err = sdl.CreateWindow(winTitle, sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
		winWidth, winHeight, sdl.WINDOW_SHOWN)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create window: %s\n", err)
		return 1
	}
	defer window.Destroy()

	renderer, err = sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create renderer: %s\n", err)
		return 2
	}
	defer renderer.Destroy()

	// Concrete
	concreteImage, err := img.Load("concrete.png")
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to load PNG: %s\n", err)
		return 3
	}
	defer concreteImage.Free()

	var concreteTexture *sdl.Texture
	concreteTexture, err = renderer.CreateTextureFromSurface(concreteImage)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create texture: %s\n", err)
		return 4
	}
	defer concreteTexture.Destroy()

	// Car icon
	carIcon, err := img.Load("car.png")
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to load PNG: %s\n", err)
		return 3
	}
	defer carIcon.Free()

	var carIconTexture *sdl.Texture
	carIconTexture, err = renderer.CreateTextureFromSurface(carIcon)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create texture: %s\n", err)
		return 4
	}
	defer carIconTexture.Destroy()

	// Load the font for our text
	var font *ttf.Font
	if font, err = ttf.OpenFont(fontPath, fontSize); err != nil {
		fmt.Println(err)
		return 5
	}
	defer font.Close()

	// Open CSV file for reading
	if len(os.Args) < 2 {
		fmt.Println("No file specified")
		return 11
	}
	csvfp, err := os.Open(os.Args[1])
	if err != nil {
		fmt.Println(err)
		return 10
	}
	defer csvfp.Close()

	startSimulation = time.Now()
	running := true

	//go dummyReader()
	go csvReader(csvfp)
	go updateCarPosition()

	for running {
		renderer.Clear()

		drawBackground(renderer, concreteTexture)

		src := sdl.Rect{0, 0, 128, 86}
		dst := sdl.Rect{int32(winWidth/2) - (128 / 2), int32(winHeight/2) - (86 / 2), 128, 86}

		rotation := math.Atan2(float64(carYSpeedSecond), float64(carXSpeedSecond))
		renderer.CopyEx(carIconTexture, &src, &dst, rotation*(180/math.Pi), nil, sdl.FLIP_NONE)

		// Create a red text with the font
		var text *sdl.Surface
		if text, err = font.RenderUTF8Blended(fmt.Sprintf("%.2f m/s", getCurrentSpeed()),
			sdl.Color{R: 255, G: 0, B: 0, A: 255}); err != nil {
			fmt.Println(err)
			return 7
		}
		defer text.Free()

		msg, err := renderer.CreateTextureFromSurface(text)
		if err != nil {
			fmt.Println(err)
			return 9
		}

		// Draw the text around the center of the window
		if err = renderer.Copy(msg, &sdl.Rect{0, 0, text.W, text.H}, &sdl.Rect{
			X: winWidth - text.W, Y: winHeight - text.H, W: text.W, H: text.H}); err != nil {
			fmt.Println(err)
			return 8
		}

		renderer.Present()

		for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
			switch t := event.(type) {
			case *sdl.QuitEvent:
				running = false
			case *sdl.KeyboardEvent:
				keyCode := t.Keysym.Sym
				keys := ""

				// Modifier keys
				switch t.Keysym.Mod {
				case sdl.KMOD_LALT:
					keys += "Left Alt"
				case sdl.KMOD_LCTRL:
					keys += "Left Control"
				case sdl.KMOD_LSHIFT:
					carXSpeedSecond += 1
					keys += "Left Shift"
				case sdl.KMOD_LGUI:
					keys += "Left Meta or Windows key"
				case sdl.KMOD_RALT:
					keys += "Right Alt"
				case sdl.KMOD_RCTRL:
					keys += "Right Control"
				case sdl.KMOD_RSHIFT:
					carXSpeedSecond -= 1
					keys += "Right Shift"
				case sdl.KMOD_RGUI:
					keys += "Right Meta or Windows key"
				case sdl.KMOD_NUM:
					keys += "Num Lock"
				case sdl.KMOD_CAPS:
					keys += "Caps Lock"
				case sdl.KMOD_MODE:
					keys += "AltGr Key"
				}

				if string(keyCode) == "q" {
					running = false
					continue
				}

				if keyCode < 10000 {
					if keys != "" {
						keys += " + "
					}

					// If the key is held down, this will fire
					if t.Repeat > 0 {
						keys += string(keyCode) + " repeating"
					} else {
						if t.State == sdl.RELEASED {
							keys += string(keyCode) + " released"
						} else if t.State == sdl.PRESSED {
							keys += string(keyCode) + " pressed"
						}
					}
				}

				if keys != "" {
					fmt.Println(keys)
				}
			}
		}

		sdl.Delay(16)
	}

	return 0
}

func main() {
	os.Exit(run())
}
