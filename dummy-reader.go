package main

import (
	"math/rand"
	"time"
)

func dummyReader() {
	rand.Seed(time.Now().Unix())
	ts := int64(0)
	for {
		time.Sleep(100 * time.Millisecond)
		ts += 100

		datachannel <- DeltaData{
			ts,
			(rand.Float64() - 0.8) * float64(rand.Intn(10)),
			(rand.Float64() - 0.6) * float64(rand.Intn(10)),
		}
	}
}
