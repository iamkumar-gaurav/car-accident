package main

import (
	"github.com/veandco/go-sdl2/sdl"
)

func drawBackground(renderer *sdl.Renderer, texture *sdl.Texture) {

	src := sdl.Rect{0, 0, 500, 500}
	initialX := carPosition.X%500 - 499
	initialY := carPosition.Y%500 - 499

	for x := initialX; x < winWidth; x += 500 {
		for y := initialY; y < winHeight; y += 500 {
			dst := sdl.Rect{x, y, 500, 500}
			renderer.Copy(texture, &src, &dst)
		}
	}
}
