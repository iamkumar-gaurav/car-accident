package main

import (
	"time"
	"encoding/csv"
	"io"
	"log"
	"strconv"
	"fmt"
)

func csvReader(fpreader io.Reader) {
	r := csv.NewReader(fpreader)

	ts := int64(0)
	skipFirstLine := true
	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}

		if skipFirstLine {
			skipFirstLine = false
			continue
		}

		accX, err := strconv.ParseFloat(record[2], 64)
		if err != nil {
			log.Fatal(err)
		}
		accY, err := strconv.ParseFloat(record[3], 64)
		if err != nil {
			log.Fatal(err)
		}

		datachannel <- DeltaData{
			ts,
			accX,
			accY,
		}

		// TODO: detect from file
		time.Sleep(200 * time.Millisecond)
		ts += 200
	}

	fmt.Println("Simulation ends")
}
