package main

import "math"

// Value expressed in m/s
var carXSpeedSecond float64 = 0
var carYSpeedSecond float64 = 0
var carPosition struct {
	X int32
	Y int32
}

func updateCarPosition() {
	oldts := int64(0)
	for {
		newdata := <-datachannel

		// Calculate time delta
		carTimeDelta := float64(newdata.Timestamp - oldts)
		oldts = newdata.Timestamp
		timeRatio := 1000.0 / carTimeDelta
		// Integrate - eg. add the speed for the specified time frame - 1/10th of seconds
		carXSpeedSecond += newdata.AccelValueX / timeRatio
		carYSpeedSecond += newdata.AccelValueY / timeRatio

		// New relative position
		carPosition.X += int32(((carXSpeedSecond * pixelPerMeter) / timeRatio))
		carPosition.Y += int32(((carYSpeedSecond * pixelPerMeter) / timeRatio))
	}
}

func getCurrentSpeed() float64 {
	return math.Sqrt(math.Pow(float64(carXSpeedSecond), 2) + math.Pow(float64(carYSpeedSecond), 2))
}
