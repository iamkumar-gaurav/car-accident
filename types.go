package main

type DeltaData struct {
	// Data timestamp (in milliseconds)
	Timestamp int64

	// Accelleration values (m/s^2)
	AccelValueX float64
	AccelValueY float64
}
